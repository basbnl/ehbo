<?php

add_action( 'wp_enqueue_scripts', 'enqueue_child_style' );

function enqueue_child_style() {
   wp_enqueue_style( 'child-style', get_stylesheet_directory_uri().'/css/main.css' );
}
