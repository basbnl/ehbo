<?php
/**
 * Template Name: Modules
 */
?>

<?php get_header(); ?>

<div id="content" class="gridlove-site-content container">
<?php if( $cover = gridlove_get_cover_layout() ) : ?>
    <?php get_template_part( 'template-parts/cover/layout-' . $cover ); ?>
<?php endif; ?>

<?php get_template_part('template-parts/ads/below-header'); ?>



    <?php $modules = gridlove_get_modules(); ?>
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-12 desc">
        <div class="gridlove-post" style="background-color: #ebebeb">
            <div class="box-inner-p">
              <?php the_content(); ?>
            </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-6 col-sm-12 desc">
        <div class="gridlove-post" style="background-color: #331b56">
          <div class="box-inner-p">
            <p class="entry-content large" style="color: white">
              <?php echo get_post_meta($post->ID, 'Uitleg', true); ?>
            </p>
          </div>
        </div>
      </div>
    </div>
    <?php if( !empty( $modules ) ): ?>

        <?php foreach( $modules as $m_ind => $module ) : $module = gridlove_parse_args( $module, gridlove_get_module_defaults( $module['type'] ) ); ?>

                <?php if ($module['active']): ?>
                    <?php $module_template = isset( $module['cpt']) ? 'cpt' : $module['type']; ?>
                    <?php include( locate_template('template-parts/modules/'.$module_template.'.php') ); ?>
                <?php endif ?>

        <?php endforeach; ?>

    <?php else: ?>

    	<?php include( locate_template('template-parts/modules/empty.php') ); ?>

    <?php endif; ?>

</div>

<?php get_footer(); ?>
