<article <?php post_class('gridlove-post gridlove-post-d gridlove-box ' . gridlove_highlight_post_class() ); ?>>
    <?php if( $eimg = get_the_post_thumbnail_url(get_the_ID(),'full') ) : ?>
      <div class="entry-image background" style='background-image:url("<?php echo $eimg; ?>"); height: 350px;'>
        <a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"></a>
      </div>
    <?php elseif( $fimg = gridlove_get_featured_image( 'd'. 6 ) ) : ?>
        <div class="entry-image">
            <a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"><?php echo $fimg; ?></a>
        </div>
    <?php endif; ?>

    <div class="entry-overlay box-inner-p">
      <div class="entry-category" style="margin-left: -30px;">
          <?php if( $icon = gridlove_get_option('lay_d_icon') ): ?>
              <?php echo gridlove_get_format_icon(); ?>
          <?php endif; ?>

          <?php if( gridlove_get_option('lay_d_cat') ) : ?>
              <?php echo gridlove_get_category(); ?>
          <?php endif; ?>
      </div>
        <div class="box-inner-ellipsis">
            <?php the_title( sprintf( '<h2 class="entry- h3"><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
            <div class="entry-category">
              <?php echo gridlove_get_excerpt(); ?>
        </div>
    </div>
    <?php if( $meta = gridlove_get_meta_data('d') ) : ?>
        <div class="entry-meta"><?php echo gridlove_get_meta_data('d'); ?></div>
    <?php endif; ?>
</article>
