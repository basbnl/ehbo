<?php
/**
 * Template Name: Blank Page (full width)
 */
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/ads/below-header'); ?>

<div id="content" class="gridlove-site-content container">

    	<div class="gridlove-content gridlove-full-width">

            <?php while( have_posts() ) : the_post(); ?>

               <article id="post-<?php the_ID(); ?>" <?php post_class('gridlove-box box-vm'); ?>>

					<div class="box-inner-p-bigger box-single">

					    <?php get_template_part('template-parts/page/entry-header'); ?>

					    <?php get_template_part('template-parts/page/entry-content'); ?>

					</div>

                </article>

            <?php endwhile; ?>

            <?php comments_template(); ?>

        </div>

</div>

<?php get_footer(); ?>

<style media="screen">
  @media screen and (min-width:640px){
  :target:before{
    content:"";
    display:block;
    padding-top:150px; /* fixed header height*/
    margin:-150px 0 0; /* negative fixed header height */
  }
  }</style>
</style>

<script type="text/javascript">

var current = 8;
function menu(){
  jQuery.each(['onzeaanpak','onzemensen','onzeervaring'], function(index, value){

    var top_of_element = jQuery("#" + value).offset().top;
    var bottom_of_element = jQuery("#" + value).offset().top + jQuery("#" + value).outerHeight();
    var bottom_of_screen = jQuery(window).scrollTop() + (jQuery(window).height() * 0.4);
    var top_of_screen = jQuery(window).scrollTop() - 100;

    if((bottom_of_screen > top_of_element) && (top_of_screen < top_of_element) && (index != current)){
        // The element is visible, do something
        console.log('heyo ' + index + ' ' + current);
        jQuery('ul.gridlove-menu li').removeClass('current-menu-item');
        jQuery('ul.gridlove-menu li:nth-of-type(' + (index + 1)  +')').addClass('current-menu-item');
        current = index;
    }
  });
}

if (jQuery('#onzeaanpak').length){
  jQuery('ul.gridlove-menu li').removeClass('current-menu-item');
  jQuery(window).load(function(){
    setTimeout(function(){

      menu();
    }, 1000);
  });
  jQuery(window).scroll(function() {
    menu();
  });
}
</script>
